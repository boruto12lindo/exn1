/*
 *
 */
package pilan1;
/**
 * @author Melina Karen Ticra AguiLar    CI  15208024
 */
public class partPila1 
{
    char arr[];
    int tope;
    
    public partPila1()
    {
        arr=new char[5];
        tope=-1;
    }

    public boolean vacio()
    {
        return(tope==-1);
    }
    public void push ( char carac)
    {
        tope++;
        if(tope<arr.length){
            arr[tope]=carac;
        }else{
            char aux[]=new char [arr.length+5];
            for(int i=0;i<arr.length;i++)
            {
                aux[i]=arr[i];
            }
            arr=aux;
            arr[tope]=carac;
        }
    }
    public char Peek()
    {
            return arr[tope];
    }
    public char pop()
    {
        if(!vacio()){
            int aux2=tope;
            tope--;
            char debT=arr[aux2];
            return debT;
        }
        else{
            return '~';
        }
    }
            
}